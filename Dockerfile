FROM node:lts-alpine AS builder

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile

COPY . .

RUN yarn build

FROM node:lts-alpine

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile --prod

COPY . .

COPY --from=builder /app/build ./build

# EXPOSE `PORT`

CMD ["node", "build/index.js"]
